using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class StartScreenManager : MonoBehaviour 
{	
	List<int> gamepadIds;

	void Start () 
	{
		gamepadIds = new List<int>();
		for(int i = 0; i < 4; i++)
			gamepadIds.Add(i+1);	
	}
	
	void Update () 
	{
		for(int i = gamepadIds.Count - 1; i >= 0; i--)
		{
			int id = gamepadIds[i];

			if(Input.GetButtonDown("Start" + id))
			{
				SceneManager.LoadScene("MainLevel");				
			}
		}
	}
}
