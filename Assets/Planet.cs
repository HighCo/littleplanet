using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Planet : MonoBehaviour {

	public float cutoff;
	public float gravityFactor;
	public float radius;
	
	void FixedUpdate () 
	{
		foreach(AffectedByGravity item in Object.FindObjectsOfType<AffectedByGravity>())
		{
			if((item.transform.position - transform.position).magnitude < cutoff)
			{
				var gravity = GetGravityVector(item.transform.position) * item.GetComponent<Rigidbody>().mass;
				item.GetComponent<Rigidbody>().AddForce(gravity);
			}
		}
	}

	public Vector3 GetGravityVector(Vector3 pos)
	{
		Vector3 delta = -(pos - transform.position) ;
		return delta / Mathf.Pow(delta.magnitude, 2) * gravityFactor;
	}

	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere(transform.position, cutoff);
	}
}
