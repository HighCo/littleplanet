using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour 
{
	public bool randomStart;
	public float speed;

	void Start()
	{
		if(randomStart)
		{
			transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
		}
	}
	
	void Update () 
	{
		transform.Rotate(new Vector3(0, 0, speed * Time.deltaTime));
	}
}
