using UnityEngine;
using System.Collections;
using System.Linq;

public class Player : MonoBehaviour {

	public int id = 1;

	public Arrow arrowPrefab;
	public GameObject[] healthIndicators;
	public SpriteRenderer alienRenderer;

	private Planet planet;
	private new Rigidbody rigidbody;

	public float inputForceScale = 10.0f;
	public float dragFactor = 10.0f;
	public float jumpFactor = 100f;
	public float speedLimit = 10.0f;
	public float invincibleDuration = 1f;
	private bool jumping = false;
	private bool grounded = false;
	private bool firing = false;
	private Animator animator;
	private int health = 3;

	public AudioClip jumpSound;
	public AudioClip hitSound;
	public AudioClip fireSound;
	public AudioClip spawnSound;
	public Transform healthBarContainer;
	public AudioSource audioSource;

	GameManager gameManager;
	float invincibleUntil;

	void Start () 
	{
		rigidbody = GetComponent<Rigidbody>();
		animator = GetComponentInChildren<Animator>();
		gameManager = FindObjectOfType<GameManager>();

		health = healthIndicators.Length;
		foreach(var item in healthIndicators)
			item.SetActive(true);
	}

	public void StartGame()
	{
		invincibleUntil = Time.time + invincibleDuration;
	}
	
	void FixedUpdate () {

		var planets = Object.FindObjectsOfType<Planet>();
		float minDistance = float.MaxValue;
		foreach(var p in planets)
		{
			var distance = (p.transform.position - transform.position).magnitude;
			if(distance < minDistance)
			{
				minDistance = distance;
				planet = p;
			}
		}

		if(planet != null)
		{
			Move();
		}
	}

	private void Move()
	{
		var input = new Vector3(Input.GetAxis("Horizontal" + id), Input.GetAxis("Vertical" + id));
		Vector3 gravity = planet.GetGravityVector(transform.position);
		Vector3 tangent = new Vector3(-gravity.y, gravity.x, 0).normalized;
		if(input.magnitude > 0.1f && gameManager.phase == GamePhase.Play)
		{
			Vector3 inputForce = Mathf.Sign(Vector3.Dot(input, tangent)) * tangent * inputForceScale;
			rigidbody.AddForce(inputForce);
			transform.localScale = new Vector3(Mathf.Sign(Vector3.Dot(input, tangent)), 1f, 1f);
			animator.SetBool("Walking", true);
		}
		else
		{
			Vector3 dragForce = -dragFactor * tangent * Vector3.Dot(tangent, rigidbody.velocity);
			rigidbody.AddForce(dragForce);
			animator.SetBool("Walking", false);
		}

		if(rigidbody.velocity.magnitude > speedLimit)
		{
			var velocityNew = rigidbody.velocity / rigidbody.velocity.magnitude * speedLimit;
			rigidbody.velocity = velocityNew;
		}
	}

	void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.GetComponent<Planet>()!=null)
		{
			Vector3 diff = transform.position + Time.fixedDeltaTime*8.0f*rigidbody.velocity - planet.transform.position;
			if(diff.magnitude <= planet.radius)
			{
				grounded = true;
				animator.SetBool("Grounded", grounded);
			}
		}
		Arrow arrow = col.gameObject.GetComponent<Arrow>();
		if(arrow != null && arrow.owner != this && Time.time >= invincibleUntil)
		{
			Destroy(col.gameObject);
			Hit();
		}
	}

	void Update()
	{
		Jump();

		Fire();

		UpdateSpriteRotation();

		if(Input.GetButtonDown("Start" + id))
		{
			if(gameManager.phase == GamePhase.Join)
			{
				gameManager.StartCountdown();
			}
			else
			if(gameManager.phase == GamePhase.GameOver)
			{
				gameManager.Reset();
			}
		}

		RenderInvincible();
	}

	private void Fire()
	{
		var aim = new Vector3(Input.GetAxis("AimX" + id), Input.GetAxis("AimY" + id));
		if(aim.magnitude > 0.7f && gameManager.phase == GamePhase.Play)
		{
			if(!firing)
			{
				Arrow arrow = (Instantiate(arrowPrefab.gameObject) as GameObject).GetComponent<Arrow>();
				arrow.transform.position = transform.position + aim.normalized * 1.1f;
				arrow.owner = this;
				arrow.GetComponent<Rigidbody>().velocity = aim.normalized * 50;
				firing = true;
				audioSource.PlayOneShot(fireSound);
			}
		}
		else
			firing = false;
	}

	private void Jump()
	{
		jumping = jumping || Input.GetButtonDown("Jump" + id);
		if(jumping)
		{
			Vector3 gravity = planet.GetGravityVector(transform.position);

			if(grounded)
			{
				rigidbody.AddForce(-gravity.normalized * jumpFactor * 10.0f);
				grounded = false;
				animator.SetBool("Grounded", grounded);
				audioSource.PlayOneShot(jumpSound);
			}
			jumping = false;
		}
	}

	void RenderInvincible()
	{
		if(Time.time < invincibleUntil)
			alienRenderer.enabled = Mathf.Floor(Time.time / .1f) % 2 == 0;
		else
			alienRenderer.enabled = true;
	}

	void UpdateSpriteRotation()
	{
		if(planet != null)
		{
			Vector3 up = transform.position - planet.transform.position;
			Quaternion rotation = Quaternion.FromToRotation(new Vector3(0, 1.0f, 0), up);
			transform.rotation = rotation;
		}
	}

	void Hit()
	{
		health--;
		invincibleUntil = Time.time + invincibleDuration;
		healthIndicators[health].SetActive(false);
		audioSource.PlayOneShot(hitSound);

		if(health <= 0)
		{
			Destroy(gameObject);
		}
	}

	public void SetMaterial(Material material)
	{
		foreach(Transform child in healthBarContainer)
			child.gameObject.GetComponent<Renderer>().material = material;
	}

	internal void Init()
	{
		audioSource.pitch = Random.Range(.8f, 1.2f);
		audioSource.PlayOneShot(spawnSound);
	}
}
