using UnityEngine;
using System.Collections;

public class PeriodicBoundary : MonoBehaviour {
	private Bounds bounds;

	// Use this for initialization
	void Start () {
		bounds.center = Camera.main.transform.position;
		bounds.extents = new Vector3(Camera.main.orthographicSize*Camera.main.aspect, Camera.main.orthographicSize, float.MaxValue);
		Debug.Log("extents: " + bounds.extents);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		var pos = transform.position;
		if(pos.x > bounds.center.x + bounds.extents.x) pos.x -= bounds.extents.x * 2; else
		if(pos.x < bounds.center.x - bounds.extents.x) pos.x += bounds.extents.x * 2; else
		if(pos.y > bounds.center.y + bounds.extents.y) pos.y -= bounds.extents.y * 2; else
		if(pos.y < bounds.center.y - bounds.extents.y) pos.y += bounds.extents.y * 2;
		transform.position = pos;
	}
}
