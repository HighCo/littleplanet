using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public enum GamePhase { Join, Countdown, Play, GameOver };

public class GameManager : MonoBehaviour 
{
	public TextMesh textMesh;
	public AudioClip winSound;
	public GameObject gameOverPanel;
	public GameObject countdownObject;

	[HideInInspector]
	public GamePhase phase;

	void Start () 
	{
		countdownObject.SetActive(false);
		phase = GamePhase.Join;
	}
	
	void Update () 
	{
		if(phase == GamePhase.Play)
		{
			var remainingPlayers = Object.FindObjectsOfType<Player>();
			if(remainingPlayers.Length == 1)
			{
				textMesh.text = string.Format("Player {0} Wins!", remainingPlayers[0].id);
				Invoke("Reset", 5);
				AudioSource.PlayClipAtPoint(winSound, transform.position);
				phase = GamePhase.GameOver;
				gameOverPanel.SetActive(true);
			}
		}

		if(Input.GetKeyDown(KeyCode.Space))
		{
			Reset();
		}
	}

	public void Reset()
	{
		SceneManager.LoadScene("MainLevel");
	}

	public void StartCountdown()
	{
		if(FindObjectsOfType<Player>().Length >= 2)
		{
			FindObjectOfType<PlayerSpawner>().gameObject.SetActive(false);
			countdownObject.SetActive(true);
			Invoke("StartGame", 3);
		}
	}

	public void StartGame()
	{
		phase = GamePhase.Play;
			
		foreach(var player in FindObjectsOfType<Player>())
			player.StartGame();
	}
}
