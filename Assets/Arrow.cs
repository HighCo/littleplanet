using UnityEngine;
using System.Collections;

public class Arrow : MonoBehaviour {

	public float lifetime = 2f;
	public Player owner;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.rotation = Quaternion.FromToRotation(new Vector3(1.0f, 0, 0), GetComponent<Rigidbody>().velocity);
		Invoke("Die", lifetime);
	}

	void Die()
	{
		Destroy(gameObject);
	}
}
