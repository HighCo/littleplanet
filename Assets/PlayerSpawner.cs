using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerSpawner : MonoBehaviour 
{
	public Player playerPrefab;
	public GameObject playerContainer;
	public Material[] materials;

	List<int> gamepadIds;
	List<SpawnPoint> spawnPoints;

	void Start () 
	{
		gamepadIds = new List<int>();
		for(int i = 0; i < 4; i++)
			gamepadIds.Add(i+1);

		spawnPoints = new List<SpawnPoint>(FindObjectsOfType<SpawnPoint>());
	}
	
	void Update () 
	{
		for(int i = gamepadIds.Count - 1; i >= 0; i-- )
		{
			int id = gamepadIds[i];

			if(Input.GetButtonDown("Jump" + id))
			{
				gamepadIds.RemoveAt(i);

				int spawnPointIndex = Random.Range(0, spawnPoints.Count);
				SpawnPoint spawnPoint = spawnPoints[spawnPointIndex];
				spawnPoints.RemoveAt(spawnPointIndex);

				Player player = Instantiate(playerPrefab.gameObject).GetComponent<Player>();
				player.transform.SetParent(playerContainer.transform);
				player.transform.position = spawnPoint.transform.position;
				player.id = id;
				player.SetMaterial(materials[id]);
				player.Init();
			}
		}
	}
}
