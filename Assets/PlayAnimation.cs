using UnityEngine;
using System.Collections;

public class PlayAnimation : MonoBehaviour 
{
	void Start () 
	{
		var animator = GetComponentInChildren<Animator>();
		animator.SetBool("Grounded", true);
		animator.SetBool("Walking", true);
	}
}
